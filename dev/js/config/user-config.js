(function() {
	'use strict';

	module.exports = ['$routeProvider', function($routeProvider) {
        $routeProvider
        .when('/', {
            templateUrl : "home.html",
            controller: 'homeController'
        })
        .when("/user", {
            templateUrl : "user.html",
            controller: 'userController'
        });
    }]
})();