
(function() {
    'use strict';

    module.exports = ['$rootScope', '$timeout', 'jQuery', function(rs, timeout, $jq){
        var link = function(s,e) {
            e = $jq(e[0]);

            rs.$on('gilt.show.menu', function() {
                console.log('asdasdfasdf');
                e.addClass('gilt-menu--show');
            });

            rs.$on('gilt.hide.menu', function() {
                e.removeClass('gilt-menu--show');
            });

            s.onClickMenuClose = function() {
                e.removeClass('gilt-menu--show');
            };

            s.menuList = [
                {name: 'new in'},
                {name: 'designer'},
                {name: 'Women'},
                {name: 'Men'},
                {name: 'Kids'},
                {name: 'Sign in'},
                {name: 'Sign up'}
            ];
        };

        return {
            restricted: 'E',
            link: link,
            scope: {},
            replace: true,
            template: [
                '<div class="gilt-menu">',
                    '<div class="gilt-menu-wrap">',
                        '<div class="gilt-menu__close ion-close-round" ng-click="onClickMenuClose()"></div>',
                        '<div ng-repeat="m in menuList">',
                            '<div class=" gilt-menu-item ion-plus">{{m.name}}</div>',
                        '</div>',
                    '</div>',
                '</div>'
            ].join('')
        };
    }]
})();