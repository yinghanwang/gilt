
(function() {
    'use strict';

    module.exports = ['$rootScope', '$timeout', 'jQuery', function(rs, timeout, $jq){
        var link = function(s,e) {
            e = $jq(e[0]);

            rs.$on('gilt.show.progress', function() {
                e.fadeIn();
            });

            rs.$on('gilt.hide.progress', function() {
                e.fadeOut();
            });
        };

        return {
            restricted: 'E',
            link: link,
            scope: {},
            replace: true,
            template: [
                '<div class="gilt-progress">',
                    '<div class="gilt-progress-wrap">',
                        '<div class="gilt-progress__spinner"></div>',
                        '<div class="gilt-progress__icon"></div>',
                    '</div>',
                '</div>'
            ].join('')
        };
    }]
})();