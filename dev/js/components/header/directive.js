
(function() {
    'use strict';

    module.exports = ['$rootScope', '$timeout', 'jQuery', function(rs, timeout, $jq){
        var link = function(s,e) {
            e = $jq(e[0]);
        };

        return {
            restricted: 'E',
            link: link,
            scope: {},
            replace: true,
            template: [
                '<div class="gilt-header">',
					'<div class="gilt-header-top d-flex justify-content-center align-items-center">',
						'<div class="gilt-header__company-name">',
							'G I L T',
						'</div>',
					'</div>',
					'<div class="gilt-header__search-container ">',
						'<div class="gilt-header__search d-flex justify-content-end align-items-center">',
							'<div class="gilt-header__search__search-ele ion-search">',
								'<input type="text" placeholder="Search">',
							'</div>',
						'</div>',
					'</div>',
				'</div>',
            ].join('')
        };
    }]
})();

