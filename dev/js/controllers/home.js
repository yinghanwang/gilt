(function() {
	'use strict';

	module.exports = [
		'$rootScope', 
		'$scope', 
		'$timeout',
		'$location',
		'userModel', function(rs, scope, timeout, location, userModel) {
		scope.userModel = userModel;

		timeout(function() {
			rs.$emit('gilt.show.menu');
		}, 3000);

		scope.onClickItem = function(item) {
			location.path('/user/').search({guid: item.guid});
		};

		function init() {
			if(!userModel.loaded) {
				showProgress();
				timeout(function() {
					scope.userModel.fetchUsers().then(createList());
				}, 1000);
			}
		}

		function createList() {
			onCreateList();
		}

		function onCreateList() {
			hideProgress();
		}

		function showProgress() {
			timeout(function() {rs.$emit('gilt.show.progress');});
		}

		function hideProgress() {
			timeout(function() {rs.$emit('gilt.hide.progress');});
		}
		
		init();
	}]
})();