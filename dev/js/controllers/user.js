(function() {
	'use strict';

	module.exports = [
		'$rootScope', 
		'$scope', 
		'$timeout',
		'$location',
		'$routeParams',
		'userModel', function(rs, scope, timeout, location, routeParams, userModel) {
		
		var userId;

		function init() {
			userId = routeParams.guid;
			scope.userData = userModel.getUserById(userId);
			if(!scope.userData) {
				location.path('/');
			}
		}
		
		scope.onClickBackBtn = function() {
			location.path('/');
		};

		init();

	}]
})();