(function() {
	'use strict';

	module.exports = ['$http', function($http) {
		return {
			data: null,
			loaded: false,
			fetchUsers: function(callback) {
				var me = this;
				return $http.get("./mock/users.json")
					.then(function(res) {
						me.data = res.data;
						me.loaded = true;
					});
			},
			getUserById: function(id){
				if(!this.data || !this.data.users) return false;
				var users = this.data.users;
				for(var i = 0, ln = users.length; i < ln; i++) {
					if(users[i].guid === id) {
						return users[i];
					}
				}
				return false;
			}
		};
	}]
})();