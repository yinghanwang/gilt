(function(angular, jQuery, undefined) {
	'use strict';

	var gilt = angular.module('gilt', ['ngRoute']);

	// application models
	var model = {
		userModel: require('./models/user-model')
	};

	var controller = {
		home: require('./controllers/home'),
		user: require('./controllers/user')
	};

	// application view componets
	var directive = {
		header: require('./components/header/directive'),
		userList: require('./components/user-list/directive'),
		userListItem: require('./components/user-list-item/directive'),
		progress: require('./components/progress/directive'),
		menu: require('./components/menu/directive')
	};

	// application router
    gilt.config(require('./config/user-config'));

	// application controller
	var homeController = gilt.controller('homeController', controller.home);
	var userController = gilt.controller('userController', controller.user);

    // wrap jquery full functionality
    gilt.factory('jQuery', function() { return jQuery; });

    // user model
	gilt.factory('userModel', model.userModel);

	// application view componenets
	// user list component
	gilt.directive('giltHeader', directive.header);
	gilt.directive('userList', directive.userList);
	gilt.directive('userItem', directive.userListItem);
	gilt.directive('giltProgress', directive.progress);
	gilt.directive('giltMenu', directive.menu);

})(angular, jQuery);