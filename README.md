# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

GILT Test

### How do I get set up? ###

Install the following packages
webpack --- npm install -g webpack
karma --- npm install -g karma

Install application dependencies
npm install

To run the app, open index.html from the public directory

To develop build js file, run
webpack --watch

To develop styling, run
gulp

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

Yinghan Wang
wangx6@gmail.com