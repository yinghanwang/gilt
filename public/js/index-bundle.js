/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	(function(angular, jQuery, undefined) {
		'use strict';

		var gilt = angular.module('gilt', ['ngRoute']);

		// application models
		var model = {
			userModel: __webpack_require__(1)
		};

		var controller = {
			home: __webpack_require__(5),
			user: __webpack_require__(7)
		};

		// application view componets
		var directive = {
			header: __webpack_require__(8),
			userList: __webpack_require__(2),
			userListItem: __webpack_require__(3),
			progress: __webpack_require__(4),
			menu: __webpack_require__(9)
		};

		// application router
	    gilt.config(__webpack_require__(6));

		// application controller
		var homeController = gilt.controller('homeController', controller.home);
		var userController = gilt.controller('userController', controller.user);

	    // wrap jquery full functionality
	    gilt.factory('jQuery', function() { return jQuery; });

	    // user model
		gilt.factory('userModel', model.userModel);

		// application view componenets
		// user list component
		gilt.directive('giltHeader', directive.header);
		gilt.directive('userList', directive.userList);
		gilt.directive('userItem', directive.userListItem);
		gilt.directive('giltProgress', directive.progress);
		gilt.directive('giltMenu', directive.menu);

	})(angular, jQuery);

/***/ },
/* 1 */
/***/ function(module, exports) {

	(function() {
		'use strict';

		module.exports = ['$http', function($http) {
			return {
				data: null,
				loaded: false,
				fetchUsers: function(callback) {
					var me = this;
					return $http.get("./mock/users.json")
						.then(function(res) {
							me.data = res.data;
							me.loaded = true;
						});
				},
				getUserById: function(id){
					if(!this.data || !this.data.users) return false;
					var users = this.data.users;
					for(var i = 0, ln = users.length; i < ln; i++) {
						if(users[i].guid === id) {
							return users[i];
						}
					}
					return false;
				}
			};
		}]
	})();

/***/ },
/* 2 */
/***/ function(module, exports) {

	(function() {
		module.exports = [function() {
			var link = function() {};

			return {
				restricted: 'E',
				link: link,
				scope: {},
				template: [

				].join(''),
			};
		}]
	})();

/***/ },
/* 3 */
/***/ function(module, exports) {

	(function() {
		module.exports = [function() {
			var link = function() {};

			return {
				restricted: 'E',
				link: link,
				scope: {},
				template: [

				].join('')
			}
		}];
	})();

/***/ },
/* 4 */
/***/ function(module, exports) {

	
	(function() {
	    'use strict';

	    module.exports = ['$rootScope', '$timeout', 'jQuery', function(rs, timeout, $jq){
	        var link = function(s,e) {
	            e = $jq(e[0]);

	            rs.$on('gilt.show.progress', function() {
	                e.fadeIn();
	            });

	            rs.$on('gilt.hide.progress', function() {
	                e.fadeOut();
	            });
	        };

	        return {
	            restricted: 'E',
	            link: link,
	            scope: {},
	            replace: true,
	            template: [
	                '<div class="gilt-progress">',
	                    '<div class="gilt-progress-wrap">',
	                        '<div class="gilt-progress__spinner"></div>',
	                        '<div class="gilt-progress__icon"></div>',
	                    '</div>',
	                '</div>'
	            ].join('')
	        };
	    }]
	})();

/***/ },
/* 5 */
/***/ function(module, exports) {

	(function() {
		'use strict';

		module.exports = [
			'$rootScope', 
			'$scope', 
			'$timeout',
			'$location',
			'userModel', function(rs, scope, timeout, location, userModel) {
			scope.userModel = userModel;

			timeout(function() {
				rs.$emit('gilt.show.menu');
			}, 3000);

			scope.onClickItem = function(item) {
				location.path('/user/').search({guid: item.guid});
			};

			function init() {
				if(!userModel.loaded) {
					showProgress();
					timeout(function() {
						scope.userModel.fetchUsers().then(createList());
					}, 1000);
				}
			}

			function createList() {
				onCreateList();
			}

			function onCreateList() {
				hideProgress();
			}

			function showProgress() {
				timeout(function() {rs.$emit('gilt.show.progress');});
			}

			function hideProgress() {
				timeout(function() {rs.$emit('gilt.hide.progress');});
			}
			
			init();
		}]
	})();

/***/ },
/* 6 */
/***/ function(module, exports) {

	(function() {
		'use strict';

		module.exports = ['$routeProvider', function($routeProvider) {
	        $routeProvider
	        .when('/', {
	            templateUrl : "home.html",
	            controller: 'homeController'
	        })
	        .when("/user", {
	            templateUrl : "user.html",
	            controller: 'userController'
	        });
	    }]
	})();

/***/ },
/* 7 */
/***/ function(module, exports) {

	(function() {
		'use strict';

		module.exports = [
			'$rootScope', 
			'$scope', 
			'$timeout',
			'$location',
			'$routeParams',
			'userModel', function(rs, scope, timeout, location, routeParams, userModel) {
			
			var userId;

			function init() {
				userId = routeParams.guid;
				scope.userData = userModel.getUserById(userId);
				if(!scope.userData) {
					location.path('/');
				}
			}
			
			scope.onClickBackBtn = function() {
				location.path('/');
			};

			init();

		}]
	})();

/***/ },
/* 8 */
/***/ function(module, exports) {

	
	(function() {
	    'use strict';

	    module.exports = ['$rootScope', '$timeout', 'jQuery', function(rs, timeout, $jq){
	        var link = function(s,e) {
	            e = $jq(e[0]);
	        };

	        return {
	            restricted: 'E',
	            link: link,
	            scope: {},
	            replace: true,
	            template: [
	                '<div class="gilt-header">',
						'<div class="gilt-header-top d-flex justify-content-center align-items-center">',
							'<div class="gilt-header__company-name">',
								'G I L T',
							'</div>',
						'</div>',
						'<div class="gilt-header__search-container ">',
							'<div class="gilt-header__search d-flex justify-content-end align-items-center">',
								'<div class="gilt-header__search__search-ele ion-search">',
									'<input type="text" placeholder="Search">',
								'</div>',
							'</div>',
						'</div>',
					'</div>',
	            ].join('')
	        };
	    }]
	})();



/***/ },
/* 9 */
/***/ function(module, exports) {

	
	(function() {
	    'use strict';

	    module.exports = ['$rootScope', '$timeout', 'jQuery', function(rs, timeout, $jq){
	        var link = function(s,e) {
	            e = $jq(e[0]);

	            rs.$on('gilt.show.menu', function() {
	                console.log('asdasdfasdf');
	                e.addClass('gilt-menu--show');
	            });

	            rs.$on('gilt.hide.menu', function() {
	                e.removeClass('gilt-menu--show');
	            });

	            s.onClickMenuClose = function() {
	                e.removeClass('gilt-menu--show');
	            };

	            s.menuList = [
	                {name: 'new in'},
	                {name: 'designer'},
	                {name: 'Women'},
	                {name: 'Men'},
	                {name: 'Kids'},
	                {name: 'Sign in'},
	                {name: 'Sign up'}
	            ];
	        };

	        return {
	            restricted: 'E',
	            link: link,
	            scope: {},
	            replace: true,
	            template: [
	                '<div class="gilt-menu">',
	                    '<div class="gilt-menu-wrap">',
	                        '<div class="gilt-menu__close ion-close-round" ng-click="onClickMenuClose()"></div>',
	                        '<div ng-repeat="m in menuList">',
	                            '<div class=" gilt-menu-item ion-plus">{{m.name}}</div>',
	                        '</div>',
	                    '</div>',
	                '</div>'
	            ].join('')
	        };
	    }]
	})();

/***/ }
/******/ ]);